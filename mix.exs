defmodule ExWindspeed.MixProject do
  use Mix.Project

  @version "0.1.0-beta"
  @repo_url "https://gitlab.com/fkumro/ex_windspeed"

  def project do
    [
      app: :ex_windspeed,
      version: @version,
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "ExWindspeed",
      docs: [
        source_ref: "v#{@version}",
        main: "ExWindspeed",
        source_url: @repo_url
      ],

      # Hex
      description: "Interface for reading data from a windspeed sensor",
      package: [
        maintainers: ["Frank Kumro"],
        licenses: ["Apache 2.0"],
        links: %{"GitLab" => @repo_url}
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {ExWindspeed.Application, []}
    ]
  end

  defp deps do
    [
      {:circuits_spi, "~> 0.1"},
      {:dialyxir, "~> 1.0.0-rc.6", only: [:dev, :test], runtime: false},
      {:erlex, "~> 0.2", only: [:dev, :test], runtime: false},
      {:stream_data, "~> 0.4.2", only: [:test]},
      {:ex_doc, "~> 0.20", only: [:dev], runtime: false},
      {:credo, "~> 1.0", only: [:dev, :test], runtime: false}
    ]
  end
end
