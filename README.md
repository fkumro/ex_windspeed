# ExWindspeed

Sensor is available for purchase from [Adafruit](https://www.adafruit.com/product/1733). If using with a Raspberry Pi a MCP3008 is required for analog -> digital conversion, also available from [Adafruit](https://www.adafruit.com/product/856).

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `ex_windspeed` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ex_windspeed, "~> 0.1.0"}
  ]
end
```

Then add `:ex_windspeed` to `application/0` to start the supervision tree.

```elixir
def application do
  [applications: [:ex_windspeed]]
end
```

## Wind speed sensor specifications

* Output: 0.4V to 2V
* Testing Range: 0.5m/s to 50m/s
* Start wind speed: 0.2 m/s
* Resolution: 0.1m/s
* Accuracy: Worst case 1 meter/s
* Max Wind Speed: 70m/s
* Connector details:
  * Pin 1 - Power (brown wire)
  * Pin 2 - Ground (black wire)
  * Pin 3 - Signal (blue wire)
  * Pin 4 not connected

## Wiring for use with a Raspberry Pi

Parts required:

* Wind speed sensor
* MCP3008

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/ex_windspeed](https://hexdocs.pm/ex_windspeed).
