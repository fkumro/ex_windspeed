defmodule ExWindspeed.Platform.RPI.SensorImpl do
  @moduledoc false
  alias ExWindspeed.Conversions
  alias ExWindspeed.Options

  @behaviour ExWindspeed.Sensor

  @impl ExWindspeed.Sensor
  @spec init(Options.t()) :: {:ok, reference()}
  def init(%Options{device: device} = _args) do
    Circuits.SPI.open(device)
  end

  @impl ExWindspeed.Sensor
  @spec read(reference(), Options.t()) ::
          {:error, ExWindspeed.Error.t()} | {:ok, ExWindspeed.t()}
  def read(ref, %Options{spi_transfer_payload: spi_transfer_payload}) when is_reference(ref) do
    ref
    |> read_volts(spi_transfer_payload)
    |> case do
      {:ok, volts} ->
        ms = Conversions.from_volts_to_ms(volts)
        mph = Conversions.from_ms_to_mph(ms)

        {:ok,
         %ExWindspeed{
           miles_per_hour: mph,
           meters_per_second: ms,
           read_at: DateTime.utc_now()
         }}

      {:error, reason} ->
        {:error, %ExWindspeed.Error{reason: reason}}
    end
  end

  defp read_volts(ref, spi_transfer_payload) when is_reference(ref) do
    case Circuits.SPI.transfer(ref, spi_transfer_payload) do
      {:ok, <<_::size(14), counts::size(10)>>} ->
        volts = Conversions.from_counts_to_volts(counts)

        if volts > 2 do
          {:error, "voltage exceeded sensor max voltage"}
        else
          {:ok, volts}
        end

      {:error, reason} ->
        {:error, reason}
    end
  end

  # 00000001
  # 10000000
  # 00000000
end
