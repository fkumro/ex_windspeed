defmodule ExWindspeed.Platform.Host.SensorImpl do
  @moduledoc false

  alias ExWindspeed.Options

  @behaviour ExWindspeed.Sensor

  @impl ExWindspeed.Sensor
  @spec init(any()) :: {:ok, reference()}
  def init(_) do
    {:ok, make_ref()}
  end

  @impl ExWindspeed.Sensor
  @spec read(any(), Options.t()) :: {:ok, ExWindspeed.t()}
  def read(_, _) do
    {:ok,
     %ExWindspeed{
       miles_per_hour: 100.00,
       meters_per_second: 44.70,
       read_at: DateTime.utc_now()
     }}
  end
end
