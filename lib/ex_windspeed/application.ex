defmodule ExWindspeed.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    opts = ExWindspeed.Options.get()

    children = [
      {ExWindspeed.Sensor, opts}
    ]

    opts = [strategy: :one_for_one, name: ExWindspeed.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
