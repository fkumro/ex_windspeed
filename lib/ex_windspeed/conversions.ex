defmodule ExWindspeed.Conversions do
  @moduledoc """
  Provides functions for converting between formats.

  ## Examples

      iex> ExWindspeed.Conversions.from_counts_to_volts(100)
      0.3225806451612903

      iex> ExWindspeed.Conversions.from_volts_to_ms(0.52)
      2.4299999999999997

      iex> ExWindspeed.Conversions.from_ms_to_mph(2.0250000000000004)
      4.53

      iex> ExWindspeed.Conversions.from_celsius_to_fahrenheit(1)
      33.8
  """

  @doc """
  Converts counts to volts
  """
  @spec from_counts_to_volts(integer) :: float
  def from_counts_to_volts(counts) when counts < 0, do: 0

  def from_counts_to_volts(counts) do
    counts / 1023 * 3.3
  end

  @doc """
  Converts volts to meters per second
  """
  @spec from_volts_to_ms(float) :: float
  def from_volts_to_ms(volts) do
    case voltage_to_ms(volts) do
      x when x >= 0 ->
        x

      x when x < 0 ->
        0
    end
  end

  @doc """
  Converts from meters per second to miles per hour. The final miles per hour
  value is trimmed to 2 decimal points.
  """
  @spec from_ms_to_mph(float) :: float
  def from_ms_to_mph(ms), do: ms |> Kernel.*(2.2369) |> trim_float

  @doc """
  Converts from celsius to fahrenheit. The final temperature value is trimmed
  to 2 decimal places.
  """
  @spec from_celsius_to_fahrenheit(float) :: float
  def from_celsius_to_fahrenheit(temp) do
    temp
    |> Kernel.*(9)
    |> Kernel./(5)
    |> Kernel.+(32)
    |> trim_float
  end

  @spec trim_float(float) :: float
  defp trim_float(float_value) do
    {trimmed_float, _} =
      :io_lib.format("~.2f", [float_value])
      |> List.to_string()
      |> Float.parse()

    trimmed_float
  end

  defp voltage_to_ms(volts) do
    (volts - 0.4) / 1.6 * 32.4
  end
end
