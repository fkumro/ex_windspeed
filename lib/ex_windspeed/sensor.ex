defmodule ExWindspeed.Sensor do
  @moduledoc false
  use GenServer

  alias ExWindspeed.Options

  @doc """
  Initialize the windspeed sensor
  """
  @callback init(opts :: Options.t()) :: {:ok, reference()}

  @doc """
  Callback to read the windspeed from the sensor
  """
  @callback read(ref :: reference(), opts :: Options.t()) ::
              {:ok, ExWindspeed.t()} | {:error, ExWindspeed.Error.t()}

  # client api
  def read(pid \\ __MODULE__) do
    GenServer.call(pid, :read)
  end

  # server api
  def start_link(args, opts \\ [name: __MODULE__]) do
    GenServer.start_link(__MODULE__, args, opts)
  end

  def init(%ExWindspeed.Options{implementation: impl} = args) do
    {:ok, ref} = impl.init(args)
    {:ok, %{impl: impl, ref: ref, config: args}}
  end

  def handle_call(:read, _from, state) do
    {:reply, state.impl.read(state.ref, state.config), state}
  end
end
