defmodule ExWindspeed.Options do
  @moduledoc """
  Configuration module built from `Application.get_all_env/1`.
  """
  defstruct implementation: ExWindspeed.Platform.RPI.SensorImpl,
            device: "spidev0.0",
            # channel 0
            spi_transfer_payload: <<0x01, 0x80, 0x00>>

  @typedoc """
  Specifies which implementation module to use.

  Current choices are:

      ExWindspeed.Platform.Host.SensorImpl
      ExWindspeed.Platform.RPI.SensorImpl - Default

  Custom modules may be used as long as they implement the `ExWindspeed.Sensor`
  behaviour.

  """
  @type implementation :: module()

  @typedoc """
  Device to read from, defaults to `spidev0.0`
  """
  @type device :: String.t()

  @typedoc """
  Bitstring to send to the MCP3008 to instruct it which channel to read.

  Defaults to channel 0

      <<0x01, 0x80, 0x00>>
  """
  @type spi_transfer_payload :: binary()

  @type t :: %__MODULE__{
          implementation: implementation,
          device: device,
          spi_transfer_payload: spi_transfer_payload
        }

  @doc """
  Gets configuration
  """
  @spec get() :: t
  def get do
    :ex_windspeed
    |> Application.get_all_env()
    |> Enum.into(%{})
    |> merge_defaults()
  end

  defp merge_defaults(settings) do
    Map.merge(%__MODULE__{}, settings)
  end
end
