defmodule ExWindspeed.Error do
  @moduledoc """
  Struct for containing errors when interacting with the sensor.
  """

  defstruct [:reason]

  @typedoc """
  Reason for failure when reading from sensor.
  """
  @type reason :: String.t()

  @typedoc """
  Struct representing a failure when reading data from the wind speed sensor.
  """
  @type t :: %__MODULE__{
          reason: reason
        }
end
