# defmodule ExWindspeed.Server do
#   @moduledoc false
#   use GenServer
#   alias ExWindspeed.Sensor
#   alias ExWindspeed.Conversions

#   # Client
#   def start_link(_) do
#     GenServer.start_link(__MODULE__, [], name: __MODULE__)
#   end

#   @spec read() :: {:ok, tuple()} | {:error, String.t()}
#   def read() do
#     GenServer.call(__MODULE__, :read)
#   end

#   # Server
#   @impl true
#   @spec init(any()) :: {:ok, %{}, {:continue, :init}}
#   def init(_) do
#     {:ok, %{}, {:continue, :init}}
#   end

#   @impl true
#   def handle_continue(:init, state) do
#     device = Application.get_env(:ex_windspeed, :device)
#     {:ok, ref} = Circuits.SPI.open(device)
#     {:noreply, %{state | ref: ref}}
#   end

#   @impl true
#   def handle_call(:read, _from, state) do
#     volts = Sensor.read(state.ref)
#     ms = volts |> Conversions.from_volts_to_ms()
#     mph = ms |> Conversions.from_ms_to_mph()

#     {:reply, {:ok, {ms, mph}}, state}
#   end
# end
